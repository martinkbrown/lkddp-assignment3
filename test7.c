
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

/**
 * This test has two cases. The first case tests that
 * a read on an empty SORT buffer with O_NONBLOCK set
 * returns immediately.
 *
 * The second case tests that a read on an empty SORT
 * but with O_NONBLOCK not set blocks and waits for data.
 * In this case, a different process B writes to the SORT
 * buffer in order to unblock process A
 */
int main() {
  int fd, result;
  char buf[10];

  // if O_NONBLOCK is set
  if ((fd = open ("/dev/scullsort", O_RDWR|O_NONBLOCK)) == -1) {
    perror("opening file");
    return -1;
  }

  result = read (fd, &buf, 1);
  buf[result] = '\0';

  // if we get there then the test has passed because it shouldn't block
  fprintf (stdout, "empty read returned from non-blocking read:\n");

  close(fd);

  // start of a new test
  // if O_NONBLOCK is not set
  if ((fd = open ("/dev/scullsort", O_RDWR)) == -1) {
    perror("opening file");
    return -1;
  }

   // child process will write to SORT after a a 5 second delay
   // to free the parent process from the blocking read
   if(fork() == 0) {

      // this read should block since there's no data
      // in the SORT buffer and O_NONBLOCK is not set
      result = read (fd, &buf, 1);
      buf[result] = '\0';
      fprintf (stdout, "\nempty read returned from blocking read\n");
   }
   else {

      int counter = 5;

      printf("Empty read is blocking, Producer will write in ");

      // "countdown timer" for the delayed write
      while(counter != -1) {

         printf("%d ", counter);
         sleep(1);
         fflush(stdout);
         counter--;

      }

      // this write should free the parent
      if ((result = write (fd, "h", 1)) != 1) {
          perror("writing");
          return -1;
      }
   }

  close(fd);

  return 0;
}
