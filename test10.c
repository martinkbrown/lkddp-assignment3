#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include "scull.h"

/**
 * This test checks the IOCTL operation. It writes
 * data to the SORT buffer. It then reads a single
 * character to show that data is really in the buffer
 *
 * It then calls ioctl() to empty the buffer, then
 * tries to read data. O_NONBLOCK is not set, so this
 * second read attempt should block.
 *
 * In order to unblock the read, a child is forked()
 * before the attempted read. The child sleeps for
 * 3 seconds, and then writes to the empty SORT
 * buffer so that the read can unblock
 */
int main() {

   int fd, result = 0;
   int quantum = 0;
   int status;
   char buf[10];

   if ((fd = open ("/dev/scullsort", O_RDWR)) == -1) {
     perror("opening file");
     return -1;
   }

   // write something to the buffer
   printf("Producer will write 'producer'\n");

   if ((result = write (fd, "producer", 8)) < 0) {
      perror("writing");
      return -1;
   }

   // read back on byte that was written
   printf("Reading 1 char ... ");

   result = read (fd, &buf, 1);
   buf[result] = '\0';

   fprintf (stdout, "read back %d: \"%s\"\n", result, buf);

   printf("Calling ioctl(%d, SCULL_IOCRESET) to empty the sort\n", fd);

   // empty the SORT buffer
   result = ioctl(fd, SCULL_IOCRESET);

   // check for possible errors from ioctl()
   if(result < 0) {

       printf("result = %d\n", result);

       switch(errno) {

       case EBADF:
          printf("  d is not a valid descriptor.\n");
       break;

       case EFAULT:
          printf("argp references an inaccessible memory area.\n");
       break;

       case EINVAL:
          printf("Request or argp is not valid.\n");
       break;

       case ENOTTY:
          printf("d is not associated with a character special device.\n");
       break;

       }

      printf("errno = %d\n", errno);
   }

   buf[0] = '\0';

   // child process tries to read, but should block since ioctl emptied the SORT buffer
   if(fork() == 0) {
      printf("Read requesting 5 chars, but buffer should be empty and should block ...\n");
      fflush(stdout);
      result = read (fd, &buf, 5);
      buf[result] = '\0';
   }
   // parent save the child, write to the SORT buffer to unblock
   else {
      sleep(3);
      printf("Forked a child process. Read should be blocking, will write 'z' 3 seconds to unblock\n");
      fflush(stdout);
      sleep(3);
      write(fd, "z", 1);
      exit(0);
   }

   if(result == 1 && buf[0] == 'z')
      fprintf (stdout, "SULL_IOCRESET successful :), read back %d: \"%s\"\n", result, buf);
   else
      printf("No data was in the buffer, SCULL_IOCRESET was NOT successful :(\n");

   close(fd);

   wait(&status);

   return 0;
}
