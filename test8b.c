
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include "scull.h"

/**
 * This test is part b of test 8. This code is
 * for the consumer. It will read n characters
 * FROm the SORT buffer, where n is user input
 * It will block if there's no data to be read
 * and O_NONBLOCK is not set. Otherwise it will
 * read until the buffer is empty and then
 * terminate.
 *
 * It should be used along with test8a.c, which
 * continuously tries to write data to SORT,
 * causing this read to unblock if it was blocked
 *
 * It doesn't terminate with O_NONBLOCK not set, 
 * exit with Ctrl+C
 *
 * Usage: ./test8b.out [0|1]
 *    0 to turn off blocking, 1 to turn on blocking
 *    blocking is off by default
 */
int main(int argc, char ** argv) {

   int fd, result;
   char buf[SCULL_S_BUFFER];

   //if O_NONBLOCK is not set
   int block = 0;

   // check command line arg
   if(argc > 1)
      if (atoi(argv[1]) == 1)
         block = 1;
      else
         block = 0;
   else
      block = 1;

   if(block)
   {
     if ((fd = open ("/dev/scullsort", O_RDWR)) == -1) {
         perror("opening file");
         return -1;
      }
   }
   else
   {
     if ((fd = open ("/dev/scullsort", O_RDWR | O_NONBLOCK)) == -1) {
         perror("opening file");
         return -1;
      }
   }

   int n;

   // keep trying to read data
   while(1) {

      n = -1;

      // let the user determine how many chars to read
      printf("How many characters to read?\n");

      // make sure n is a valid number
      while(n < 0) {
         scanf("%d", &n);
      }

      result = read (fd, &buf, n);
      buf[result] = '\0';

      if(result > 0)
         fprintf (stdout, "read back %d: \"%s\"\n", result, buf);
   }

   close(fd);

   return 0;
}
