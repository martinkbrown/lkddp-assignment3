
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

/**
 * This test is part a of test 8. This code is
 * for the producer. It will write to SORT buffer
 * until it is full, then the write will block
 * if O_NONBLOCK is not set. Otherwise it will
 * write until the buffer is full and then
 * terminate.
 *
 * It should be used along with test8b.c, which
 * asks the user how many characters to consume
 * from SORT, causing this write to unblock
 * and then block again.
 *
 * It doesn't terminate with O_NONBLOCK not set, 
 * exit with Ctrl+C
 *
 * Usage: ./test8a.out [0|1]
 *    0 to turn off blocking, 1 to turn on blocking
 *    blocking is off by default
 */
int main(int argc, char ** argv) {
  int fd, result;
  char buf[10];

    //if O_NONBLOCK is not set (if BLOCK is set)
   int block = 0;

   // check args to determine whether or not to set O_NONBLOCK
   if(argc > 1)
      if (atoi(argv[1]) == 1)
         block = 1;
      else
         block = 0;
   else
      block = 1;

   // open with blocking
   if(block)
   {
     if ((fd = open ("/dev/scullsort", O_RDWR)) == -1) {
       perror("opening file");
       return -1;
     }
   }

   // open with no blocking
   else
   {
      if ((fd = open ("/dev/scullsort", O_RDWR | O_NONBLOCK)) == -1) {
         perror("opening file");
         return -1;
      }
   }

   /**
    * cdata contains the data that will be written
    */
   char cdata = 'z';
   char data[1];

   // keep writing data
   while(1) {

      data[0] = cdata;

      if ((result = write (fd, data, 1)) != 1) {
         perror("writing");
         return -1;
      }

      printf("%c", data[0]);
      fflush(stdout);

      // write a different character next time
      cdata--;

      // reset cdata
      if(cdata < 'A')
         cdata = 'z';

   }

   close(fd);

   return 0;
}
