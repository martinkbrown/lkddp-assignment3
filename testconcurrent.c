#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <sys/ioctl.h>

#define SCULL_IOC_MAGIC  '-'
/* Please use a different 8-bit number in your code */

#define SCULL_IOCRESET    _IO(SCULL_IOC_MAGIC, 0)

#define NUM_CHILDREN 10

int cid = 1;

/**
 * This test checks concurrent reading and writing,
 * including calls to the ioctl() routine. The parent
 * process forks NUM_CHILDREN children. Some of the children
 * write, some read, and one does ioctl().
 *
 * By default, it does 2 iterations for this reading
 * and writing, but a command line arg can be
 * specified to change the number of iterations
 *
 * Usage: ./testconcurrent.out [i]
 *    Where i is the number of iterations
 *
 * Random strings of random lengths are read and written
 */
int main(int argc, char ** argv) {

   int fd, result = 0;
   int quantum = 0;

   // used for srand() initialization
   time_t seconds;

   // the number of characters to read
   int rCount = 0;

   int i, pid, status, return_id;

   int iterations = 2;

   // check for number of iterations command line arg
   if(argc == 2)
      iterations = atoi(argv[1]);

   // parent process will fork() NUM_CHILDREN children
   for(i = 0; i < NUM_CHILDREN; i++) {

      pid = fork();

      // is this a child process?
      if(pid == 0) {

         // give each child a "pid", pid local to this code
         pid = cid;

         // children will break so that they don't re-enter the loop
         // loop is only for parent to fork() children
         break;
      }

      // increase the global child id counter to assign to the
      // next child
      cid++;

      // the "pid" of the parent will be zero
      pid = 0;
   }

   if ((fd = open ("/dev/scullsort", O_RDWR | O_NONBLOCK)) == -1) {
      perror("opening file");
      return -1;
   }

   // the parent shouldn't enter this block
   if(pid != 0)
   {

   // init the random number generator
   time(&seconds);
   srand((unsigned int) seconds + pid);
   char buf[4000];

   for(i = 0; i < iterations; i++)
   {
      buf[0] = '\0';

      // have each child sleep for a random interval between 0-5 seconds
      // so that race is more interleaved on each run
      sleep(rand() % 5);

      // this special child process is the only on that ioctls
      if(pid % NUM_CHILDREN == 0) {

         result = ioctl(fd, SCULL_IOCRESET);

         if(result >= 0) {
            printf("\n[RESETTER %d] reset the buffer\n", pid);
         }

      }
      // this set of child processes write random sized strings
      // (length between 0 and 20) to the SORT buffer
      else if(pid % 2 == 0)
      {

         char randChar = 0;
         int wordsize = 0;

         // size of a random string
         wordsize = rand() % 20;

         // build the random string one char at a time
         for(i = 0; i < wordsize; i++) {
            while(randChar < 33)
               randChar = rand() % 126;

            buf[i] = randChar;
            randChar = 0;
         }

         // null character for the generated string
         buf[i] = '\0';

         if ((result = write (fd, buf, wordsize)) < 0) {
            perror("writing");
            return -1;
         }

         printf("\n[PRODUCER %d] tried to write %d and wrote %d \"%s\"\n", pid, wordsize, result, buf);

      }
      // this set of child processes consume data from the SORT buffer
      else {
         rCount = rand() % 20;

         result = read (fd, &buf, rCount);
         buf[result] = '\0';

         fprintf (stdout, "\n[CONSUMER %d] tried to read %d and read %d \"%s\"\n", pid, rCount, result, buf);
      }
   }
   }

   // have the children exit safely
   if(pid != 0) {
      close(fd);
      exit(0);
   }

   // parent should wait for each child to exit
   for(i = 0; i < NUM_CHILDREN; i++) {

      return_id = wait(&status);
   }

   // parent resets the SORT buffer
   result = ioctl(fd, SCULL_IOCRESET);

   close(fd);

   printf("\n");

   return 0;
}
