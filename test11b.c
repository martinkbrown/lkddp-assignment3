
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

/**
 * This is part b of test11. It simply opens
 * the device and reads data, if there is any.
 * It's made to be used right after test11a
 * to show that the SORT buffer data is preserved
 * even though there are no more
 * readers and writers to the device before
 * this process starts
 */
int main() {
   int fd, result;
   char buf[10];

   //if O_NONBLOCK is not set

   printf("Consumer do open and read contents\n");

   if ((fd = open ("/dev/scullsort", O_RDWR)) == -1) {
     perror("opening file");
     return -1;
   }

   result = read (fd, &buf, 10);
   buf[result] = '\0';

   fprintf (stdout, "read back %d: \"%s\"\n", result, buf);

   close(fd);

   return 0;
}
