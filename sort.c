/*
 * sort.c -- fifo driver for scull
 *
 * Copyright (C) 2001 Alessandro Rubini and Jonathan Corbet
 * Copyright (C) 2001 O'Reilly & Associates
 *
 * The source code in this file can be freely used, adapted,
 * and redistributed in source or binary form, so long as an
 * acknowledgment appears in derived source files.  The citation
 * should list that the code comes from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   No warranty is attached;
 * we cannot take responsibility for errors or fitness for use.
 *
 */
 
#include <linux/module.h>
#include <linux/moduleparam.h>

#include <linux/kernel.h>	/* printk(), min() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/proc_fs.h>
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/fcntl.h>
#include <linux/poll.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/sched.h>

#include <linux/mutex.h>

#include "scull.h"		/* local definitions */

/**
 * scull sort structure is almost identical to the pipe structure
 */
struct scull_sort {

        /**
         * read queues
         */
        wait_queue_head_t inq;

        /**
         * write queue
         */
        wait_queue_head_t outq;

        /**
         * begin of buf
         */
        char *buffer;

        /**
         * end of buf
         */
        char *end;

        /**
         * used in pointer arithmetic
         */
        int buffersize;

        /**
         * where to read
         */
        char *rp;

        /**
         * where to write
         */
        char *wp;

        /**
         *  number of openings for r
         */
        int nreaders;

        /**
         *  number of openings for w
         */
        int nwriters;

        /**
         * asynchronous readers
         */
        struct fasync_struct *async_queue;

        /**
          * mutual exclusion mutex
         */
        struct mutex mut;

        /**
         * Char device structure
         */
        struct cdev cdev;
};

/* parameters */
static int scull_s_nr_devs = SCULL_S_NR_DEVS;	/* number of sort devices */
int scull_s_buffer =  SCULL_S_BUFFER;	/* buffer size */
dev_t scull_s_devno;			/* Our first device number */

module_param(scull_s_nr_devs, int, 0);	/* FIXME check perms */
module_param(scull_s_buffer, int, 0);

static struct scull_sort *scull_s_devices;

static int scull_s_fasync(int fd, struct file *filp, int mode);
static int spacefree(struct scull_sort *dev);
/*
 * Open and close
 */


static int scull_s_open(struct inode *inode, struct file *filp)
{
	struct scull_sort *dev;

	dev = container_of(inode->i_cdev, struct scull_sort, cdev);
	filp->private_data = dev;

	if (mutex_lock_interruptible(&dev->mut))
		return -ERESTARTSYS;
	if (!dev->buffer) {

		/* allocate the buffer */
		dev->buffer = kmalloc(scull_s_buffer, GFP_KERNEL);
		if (!dev->buffer) {
			mutex_unlock(&dev->mut);
			return -ENOMEM;
		}

           dev->buffersize = scull_s_buffer;
           dev->end = dev->buffer + dev->buffersize;

           dev->rp = dev->wp = dev->buffer; /* rd and wr from the beginning */
	}

	/* use f_mode,not  f_flags: it's cleaner (fs/open.c tells why) */
	if (filp->f_mode & FMODE_READ)
		dev->nreaders++;
	if (filp->f_mode & FMODE_WRITE)
		dev->nwriters++;
	mutex_unlock(&dev->mut);

	return nonseekable_open(inode, filp);
}

static int scull_s_release(struct inode *inode, struct file *filp)
{
	struct scull_sort *dev = filp->private_data;

	/* remove this filp from the asynchronously notified filp's */
	scull_s_fasync(-1, filp, 0);
	mutex_lock(&dev->mut);
	if (filp->f_mode & FMODE_READ)
		dev->nreaders--;
	if (filp->f_mode & FMODE_WRITE)
		dev->nwriters--;

        // don't free  the buffer even if there are no more readers or writers
        // the code to free the buffer has been removed

	mutex_unlock(&dev->mut);
	return 0;
}


/**
 * @param[*filp] fil pointer
 * @param[*buf]  the user's buffer that data will be written to
 * @param[count] the number of bytes to read
 */
static ssize_t scull_s_read (struct file *filp, char __user *buf, size_t count,
                loff_t *f_pos)
{
        struct scull_sort *dev = filp->private_data;

	if (mutex_lock_interruptible(&dev->mut))
		return -ERESTARTSYS;

	while (dev->rp == dev->wp) { /* nothing to read */
		mutex_unlock(&dev->mut); /* release the lock */
		if (filp->f_flags & O_NONBLOCK)
			return -EAGAIN;
		PDEBUG("\"%s\" reading: going to sleep\n", current->comm);
		if (wait_event_interruptible(dev->inq, (dev->rp != dev->wp)))
			return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
		/* otherwise loop, but first reacquire the lock */
		if (mutex_lock_interruptible(&dev->mut))
			return -ERESTARTSYS;
	}

	/* ok, data is there, return something */
	if (dev->wp > dev->rp)
        {
		count = min(count, (size_t)(dev->wp - dev->rp));
                do_sort(dev->rp, (int) (dev->wp - dev->rp));
        }
	else /* the write pointer has wrapped, return data up to dev->end */
        {
		count = min(count, (size_t)(dev->end - dev->rp));
                do_sort(dev->rp, (int) (dev->end - dev->rp));
        }

	if (copy_to_user(buf, dev->rp, count)) {
		mutex_unlock(&dev->mut);
		return -EFAULT;
	}
	dev->rp += count;
	if (dev->rp == dev->end)
		dev->rp = dev->buffer; /* wrapped */
	mutex_unlock(&dev->mut);

	/* finally, awake any writers and return */
	wake_up_interruptible(&dev->outq);
	PDEBUG("\"%s\" did read %li bytes\n",current->comm, (long)count);
	return count;
}

/* Wait for space for writing; caller must hold device mutex.  On
 * error the mutex will be released before returning. */
static int scull_getwritespace(struct scull_sort *dev, struct file *filp, int count)
{
	while (spacefree(dev) < count) { /* no space */
		DEFINE_WAIT(wait);

		mutex_unlock(&dev->mut);
		if (filp->f_flags & O_NONBLOCK)
			return -EAGAIN;
		PDEBUG("\"%s\" writing: going to sleep\n",current->comm);
		prepare_to_wait(&dev->outq, &wait, TASK_INTERRUPTIBLE);
		if (spacefree(dev) < count)
			schedule();
		finish_wait(&dev->outq, &wait);
		if (signal_pending(current))
			return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
		if (mutex_lock_interruptible(&dev->mut))
			return -ERESTARTSYS;
	}
	return 0;
}

/* How much space is free? */
static int spacefree(struct scull_sort *dev)
{
	if (dev->rp == dev->wp)
		return dev->buffersize - 1;
	return ((dev->rp + dev->buffersize - dev->wp) % dev->buffersize) - 1;
}

static ssize_t scull_s_write(struct file *filp, const char __user *buf, size_t count,
                loff_t *f_pos)
{
	struct scull_sort *dev = filp->private_data;
	int result;
        int freespace = 0;
	if (mutex_lock_interruptible(&dev->mut))
		return -ERESTARTSYS;

	/* Make sure there's space to write */

        freespace = spacefree(dev);

	result = scull_getwritespace(dev, filp, count);
	if (result)
		return result; /* scull_getwritespace called mutex_unlock(&dev->mut) */

	/* ok, space is there, accept something */
	count = min(count, (size_t)spacefree(dev));
	if (dev->wp >= dev->rp)
		count = min(count, (size_t)(dev->end - dev->wp)); /* to end-of-buf */
	else /* the write pointer has wrapped, fill up to rp-1 */
		count = min(count, (size_t)(dev->rp - dev->wp - 1));
	PDEBUG("Going to accept %li bytes to %p from %p\n", (long)count, dev->wp, buf);
	if (copy_from_user(dev->wp, buf, count)) {
		mutex_unlock(&dev->mut);
		return -EFAULT;
	}
	dev->wp += count;
	if (dev->wp == dev->end)
		dev->wp = dev->buffer; /* wrapped */
	mutex_unlock(&dev->mut);

	/* finally, awake any reader */
	wake_up_interruptible(&dev->inq);  /* blocked in read() and select() */

	/* and signal asynchronous readers, explained late in chapter 5 */
	if (dev->async_queue)
		kill_fasync(&dev->async_queue, SIGIO, POLL_IN);
	PDEBUG("\"%s\" did write %li bytes\n",current->comm, (long)count);
//printk(KERN_ALERT "spacefree = %d\n", freespace);
	return count;
}

static int scull_s_fasync(int fd, struct file *filp, int mode)
{
	struct scull_sort *dev = filp->private_data;

	return fasync_helper(fd, filp, mode, &dev->async_queue);
}



/* FIXME this should use seq_file */
#ifdef SCULL_DEBUG
static void sculls_proc_offset(char *buf, char **start, off_t *offset, int *len)
{
	if (*offset == 0)
		return;
	if (*offset >= *len) {	/* Not there yet */
		*offset -= *len;
		*len = 0;
	}
	else {			/* We're into the interesting stuff now */
		*start = buf + *offset;
		*offset = 0;
	}
}


static int scull_read_s_mem(char *buf, char **start, off_t offset, int count,
		int *eof, void *data)
{
	int i, len;
	struct scull_sort *p;

#define LIMIT (PAGE_SIZE-200)	/* don't print any more after this size */
	*start = buf;
	len = sprintf(buf, "Default buffersize is %i\n", scull_s_buffer);
	for(i = 0; i<scull_s_nr_devs && len <= LIMIT; i++) {
		p = &scull_s_devices[i];
		if (mutex_lock_interruptible(&p->sem))
			return -ERESTARTSYS;
		len += sprintf(buf+len, "\nDevice %i: %p\n", i, p);
/*		len += sprintf(buf+len, "   Queues: %p %p\n", p->inq, p->outq);*/
		len += sprintf(buf+len, "   Buffer: %p to %p (%i bytes)\n", p->buffer, p->end, p->buffersize);
		len += sprintf(buf+len, "   rp %p   wp %p\n", p->rp, p->wp);
		len += sprintf(buf+len, "   readers %i   writers %i\n", p->nreaders, p->nwriters);
		mutex_unlock(&p->sem);
		sculls_proc_offset(buf, start, &offset, &len);
	}
	*eof = (len <= LIMIT);
	return len;
}


#endif



/*
 * The file operations for the sort device
 * (some are overlayed with bare scull)
 */
struct file_operations scull_sort_fops = {
	.owner =	THIS_MODULE,
	.llseek =	no_llseek,
	.read =		scull_s_read,
	.write =	scull_s_write,
	.unlocked_ioctl = scull_ioctl,
	.open =		scull_s_open,
	.release =	scull_s_release,
	.fasync =	scull_s_fasync,
};


/*
 * Set up a cdev entry.
 */
static void scull_s_setup_cdev(struct scull_sort *dev, int index)
{
	int err, devno = scull_s_devno + index;
    
	cdev_init(&dev->cdev, &scull_sort_fops);
	dev->cdev.owner = THIS_MODULE;
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding scullsort%d", err, index);
}

 

/*
 * Initialize the sort devs; return how many we did.
 */
int scull_s_init(dev_t firstdev)
{
	int i, result;

	result = register_chrdev_region(firstdev, scull_s_nr_devs, "sculls");
	if (result < 0) {
		printk(KERN_NOTICE "Unable to get sculls region, error %d\n", result);
		return 0;
	}
	scull_s_devno = firstdev;
	scull_s_devices = kmalloc(scull_s_nr_devs * sizeof(struct scull_sort), GFP_KERNEL);
	if (scull_s_devices == NULL) {
		unregister_chrdev_region(firstdev, scull_s_nr_devs);
		return 0;
	}
	memset(scull_s_devices, 0, scull_s_nr_devs * sizeof(struct scull_sort));
	for (i = 0; i < scull_s_nr_devs; i++) {
		init_waitqueue_head(&(scull_s_devices[i].inq));
		init_waitqueue_head(&(scull_s_devices[i].outq));
		mutex_init(&scull_s_devices[i].mut);
		scull_s_setup_cdev(scull_s_devices + i, i);
	}
#ifdef SCULL_DEBUG
	create_proc_read_entry("scullsort", 0, NULL, scull_read_s_mem, NULL);
#endif
	return scull_s_nr_devs;
}

/*
 * This is called by cleanup_module or on failure.
 * It is required to never fail, even if nothing was initialized first
 */
void scull_s_cleanup(void)
{
	int i;

#ifdef SCULL_DEBUG
	remove_proc_entry("scullsort", NULL);
#endif

	if (!scull_s_devices)
		return; /* nothing else to release */

	for (i = 0; i < scull_s_nr_devs; i++) {
		cdev_del(&scull_s_devices[i].cdev);
		kfree(scull_s_devices[i].buffer);
	}
	kfree(scull_s_devices);
	unregister_chrdev_region(scull_s_devno, scull_s_nr_devs);
	scull_s_devices = NULL; /* pedantic */
}

char *my_strcpy(char *dest, const char *src)
{
   return my_strncpy(dest, src, 0);
}

char *my_strncpy(char *dest, const char *src, size_t n)
{
   size_t i;

   if(dest == NULL || src == NULL)
      return dest;

   for (i = 0 ; (i < n || n == 0) && src[i] != '\0' ; i++)
      dest[i] = src[i];

   for ( ; i < n ; i++)
      dest[i] = '\0';

   return dest;
}


void do_sort(char *to_sort, int size)
{
   int i = 0;
   int swapped = 1;
   char temp;

   while(swapped) {

      swapped = 0;

      for(i = 1; i < size; i++) {

         if(((unsigned int) to_sort[i-1]) > ((unsigned int) to_sort[i])) {
            temp = to_sort[i-1];
            to_sort[i-1] = to_sort[i];
            to_sort[i] = temp;
            swapped = 1;
         }

      }
   }
}

int scull_s_empty(struct file *filp)
{
        struct scull_sort *dev = filp->private_data;

        char * c;

        if (mutex_lock_interruptible(&dev->mut))
                return -ERESTARTSYS;

        for(c = dev->buffer; c != dev->end; c++) {
           *c = ' ';
        }

        dev->rp = dev->wp = dev->buffer;

        mutex_unlock(&dev->mut);

        /* finally, awake any reader */
        wake_up_interruptible(&dev->inq);  /* blocked in read() and select() */

        /* and signal asynchronous readers, explained late in chapter 5 */
        if (dev->async_queue)
                kill_fasync(&dev->async_queue, SIGIO, POLL_IN);

        return 0;
}

