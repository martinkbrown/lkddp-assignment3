
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

/**
 * Simple test. This test writes "hgfed" to the SORT buff,
 * followed by cba. It then tries to read 5 characters.
 * The read should fill buf with defgh. A subsequent read
 * of 5 characters should fill buf with abc since
 * only 3 characters are left in the SORT buffer
 */
int main() {
  int fd, result;
  char buf[10];

  if ((fd = open ("/dev/scullsort", O_RDWR)) == -1) {
    perror("opening file");
    return -1;
  }
  if ((result = write (fd, "hgfed", 5)) != 5) {
    perror("writing");
    return -1;
  }

  printf("write \"hgfed\"\n");

  if ((result = write (fd, "cba", 3)) != 3) {
    perror("writing");
    return -1;
  }

  printf("write \"cba\"\n");

  result = read (fd, &buf, 5);
  buf[result] = '\0';
  fprintf (stdout, "read back %d: \"%s\"\n", result, buf);

  result = read (fd, &buf, 5);
  buf[result] = '\0';
  fprintf (stdout, "read back %d: \"%s\"\n", result, buf);

  close(fd);
  return 0;
}
