#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

/**
 * This is part a of test11. It should be used along
 * with test11b, but prior to test11b, or with
 * > cat /dev/scullsort
 *
 * This producer writes data to the SORT buffer then
 * closes the file descriptor. The SORT buffer should
 * not be made empty as a result.
 *
 * Now run test11b, which will open the device and
 * then read the data that was written by this producer
 */
int main() {
   int fd, result;
   char buf[10];

   if ((fd = open ("/dev/scullsort", O_RDWR)) == -1) {
     perror("opening file");
     return -1;
   }

   printf("Producer will write 'producer'\n");

   if ((result = write (fd, "producer", 8)) < 0) {
      perror("writing");
      return -1;
   }

   // fd is closed but data should be preserved for the
   // next process that opens the device
   close(fd);

   printf("Producer did close()\n");

   return 0;
}
